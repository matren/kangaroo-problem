import { Component } from '@angular/core';
import { Kangaroo, Tiger } from './animal-kingdom/my-animals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'kangaroo';

  public myKangaroo = new Kangaroo(2, 1, true);
  public myTiger = new Tiger(2, 1);
}
