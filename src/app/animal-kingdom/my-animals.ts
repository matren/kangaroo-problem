export enum AnimalType {
  Tiger,
  Kagaroo
}

export abstract class Animal {
  public abstract kind: string;
  public eyes = 2;
  public nose: any;

  constructor(eyes, nose) {
    this.eyes = eyes;
    this.nose = nose;
  }

  public canSee(): string {
    return 'I see a lion!';
  }

  public canSmell(): string {
    return 'I smell nothing';
  }

  abstract handleBaby(baby: any): void;
}

export class Kangaroo extends Animal {
  public kind: 'Kangaroo' = 'Kangaroo';
  public bag: any[] = [];

  constructor(eyes, nose, withBaby: boolean) {
    super(eyes, nose);
    if (withBaby) {
      this.putSomethingInsideTheBag('babykangaroo');
    }
  }

  public putSomethingInsideTheBag(something: any): void {
    this.bag.push(something);
  }

  public handleBaby(baby: any): void
  {
    this.putSomethingInsideTheBag(baby)
  }
}

export class Tiger extends Animal {
  public kind: 'Tiger' = 'Tiger';

  constructor(eyes, nose) {
    super(eyes, nose);
  }

  public ROOOAAAAR(): string {
    return 'ROOOOOOARR';
  }

  public handleBaby(baby: any)
  {
    this.eat(baby);
    this.ROOOAAAAR();
  }

  private eat(foot: any): void
  {
    // ... mampf
  }
}
