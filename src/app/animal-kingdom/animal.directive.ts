import { Directive, Input, HostListener } from '@angular/core';
import { Animal, Kangaroo, Tiger } from './my-animals';

/* Lösung A: instanceOf*/
export class AnimalDirective {
  @Input()
  public appAnimal: Animal;

  constructor() {
    console.log(this.appAnimal);
  }

  @HostListener('click')
  public onClick(): void {

    if (this.appAnimal instanceof Kangaroo) {
      // durch die instanceof prüfung kennt typescript nun den konkreten typ und erlaub dir den zugriff auf alle eigenschaften von kangaroo.
      this.appAnimal.putSomethingInsideTheBag('egg');
    }
    else {
      console.log(`An unspecific animal with ${this.appAnimal.eyes} eyes`)
    }
  }
}

/*Lösung B: tagged union */
@Directive({
  selector: '[appAnimal]'
})
export class AnimalDirective2 {
  @Input()
  public appAnimal: Kangaroo | Tiger;

  constructor() {
    console.log(this.appAnimal);
  }

  @HostListener('click')
  public onClick(): void {
    
    if (this.appAnimal.kind === 'Kangaroo') {
      // durch prüfung der eigenschaft "kind" kennt typescript nun den konkreten typ und erlaub dir den zugriff auf alle eigenschaften von kangaroo, bzw. Tiger.
      this.appAnimal.putSomethingInsideTheBag('egg');
    }
    else {
      this.appAnimal.ROOOAAAAR();
    }
  }
}

/* Lösung C: shift logic from ui to business class*/
@Directive({
  selector: '[appAnimal]'
})
export class AnimalDirective3 {
  @Input()
  public appAnimal: Animal;

  constructor() {
    console.log(this.appAnimal);
  }

  @HostListener('click')
  public onClick(): void {
    
    // logik verbleibt in der jeweiligen implementierung - ui ist dumm (beste lösung)
    // type-checking sollte vermieden werden, sonst ist die klasse sehr schwer zu testen.
    const baby: any = 'baby peter';
    this.appAnimal.handleBaby(baby);
  }
}